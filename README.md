
# DESCRIPTION 
Item Management Page

Manages items in two different lists
There is a text box to add Item name. On clicking Add Item will be added to the box below the list and input is cleared.
User cannot add same item name. On entering same name alert message appear.
User can select one item at a time and move item form left to right using '->' button or right to left using '->' button.
The state of the application is stored in backend MySQL database. If user closes or load the page, data is not lost.

# REQUIREMENTS 
- php version  7.3.13

# STEPS TO RUN
- git clone https://hillsofttech@bitbucket.org/zasyadev/itemsmanagementtest.git
- composer install
- php artisan key:generate   
- php atisan migrate
- to run on localhost :  
    - cd ItemManagementTest 
    - php artisan serve    
    
    




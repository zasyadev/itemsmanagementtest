<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('getItems', [App\Http\Controllers\ItemController::class, 'getItems'])->name('items');
Route::post('addItems', [App\Http\Controllers\ItemController::class, 'addItems'])->name('add');

Route::post('shiftToRightItems', [App\Http\Controllers\ItemController::class, 'shiftRight'])->name('shiftRight');

Route::post('shiftToLeftItems', [App\Http\Controllers\ItemController::class, 'shiftLeft'])->name('shiftLeft');

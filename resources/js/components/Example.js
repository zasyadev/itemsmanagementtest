import React from 'react';
import ReactDOM from 'react-dom';
import style from './style.css';
import List from './list.js';


function Example() {

    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <div className="card">
                        <h2 className="card-header">Items Management Page</h2>

                        <div className="card-body" style={{style }}>
                            <List></List>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    );
}

export default Example;

if (document.getElementById('example')) {
    ReactDOM.render(<Example />, document.getElementById('example'));
}

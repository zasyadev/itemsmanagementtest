import React, { Component } from 'react';
import style from './style.css';

export default class List extends Component {

  constructor(props){
    super(props);
    this.saveButton = this.saveButton.bind(this);
    this.rightItemClick = this.rightItemClick.bind(this);
    this.leftItemClick = this.leftItemClick.bind(this);
    this.rightToLeftButtonClick = this.rightToLeftButtonClick.bind(this);
    this.lefttoRightButtonClick = this.lefttoRightButtonClick.bind(this); 
    this.state = {
      item : '',
      selectedItem : '',
      list1_items :'',
      list2_items : '',
      selectedItemLeft :'',
      color: 'white',
      color1 : 'white',
      leftToRightbtnDisabled :false,
      // rightToLeftbtnDisabled :false

    }
  }

componentWillMount () {
    const headers = new Headers({
        "Accept": "application/json",
    });
    let url ='/getItems';
    fetch(url, headers)
        .then(response => response.json())
        .then(data => this.setState({ list1_items: data.left_items,list2_items: data.right_items
       })) .catch(error => {
        this.setState({ errorMessage: error.toString() });
        console.error('There was an error!', error);
    });;
       
    } 
    saveButton() { 
      const headers = new Headers({
        "Accept": "application/json",
      });
        if(this.state.list1_items.some(val => val.item_name === this.state.item) || this.state.list2_items.some(val => val.item_name === this.state.item)){
            alert("Item already exists");
        }else{

          const re = new RegExp('<meta name="csrf-token" content="(.*)" />')
          const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ item: this.state.item,  })
          };
          let url ='/addItems';
          fetch(url, requestOptions)
            .then(response => response.json())
            .then(
              fetch('/getItems', headers)
              .then(response => response.json())
              .then(data => this.setState({ list1_items: data.left_items,list2_items: data.right_items,item: '' }))
          );
        }
      
    };
    
  
    rightToLeftButtonClick() { 
      if( !this.state.list2_items.some(val => val.id === this.state.selectedItem) ){
        if(this.state.selectedItem != ''){
          this.setState({ rightToLeftbtnDisabled :true});
          const headers = new Headers({
            "Accept": "application/json",
          });
          
            const re = new RegExp('<meta name="csrf-token" content="(.*)" />')
            const requestOptions = {
              method: 'POST',
              headers: { 'Content-Type': 'application/json' },
              body: JSON.stringify({ item_id: (this.state.selectedItem)  })
            };
            let url ='/shiftToRightItems';
           
            fetch(url, requestOptions)
              .then(response => response.json())
              .then(
                fetch('/getItems', headers)
                .then(response => response.json())
                .then(data => this.setState({ list1_items: data.left_items,list2_items: data.right_items,item: '',rightToLeftbtnDisabled :false }))
            ); 
 
        }
      }
    };

    leftItemClick(item) {    
      if(this.state.color1 == 'white') {
        this.setState({ color:'#9e9e9e',selectedItem: item.id});
      }else{
        this.setState({ color1:'white',selectedItemLeft:'',color:'#9e9e9e',selectedItem: item.id});
      
      } 
    }
    rightItemClick(item) {    
      if(this.state.color == 'white') {
        this.setState({ color1:'#9e9e9e', selectedItemLeft : item.id });
      }else{
        this.setState({ color:'white',selectedItem:'',color1:'#9e9e9e',selectedItemLeft: item.id});
      }
    };

    lefttoRightButtonClick() {  
      if( !this.state.list1_items.some(val => val.id === this.state.selectedItemLeft) ){
        if(this.state.selectedItemLeft != ''){
          this.setState({ leftToRightbtnDisabled :true});

          const headers = new Headers({
            "Accept": "application/json",
          });
            const re = new RegExp('<meta name="csrf-token" content="(.*)" />')
            const requestOptions = {
              method: 'POST',
              headers: { 'Content-Type': 'application/json' },
              body: JSON.stringify({ item_id: (this.state.selectedItemLeft)  })
            };
            let url ='/shiftToLeftItems';
            fetch(url, requestOptions)
              .then(response => response.json())
              .then(
                fetch('/getItems', headers)
                .then(response => response.json())
                .then(data => this.setState({ list1_items: data.left_items,list2_items: data.right_items,item: '', leftToRightbtnDisabled :false }))
            ); 
           
        }
      }
      
    };



  render() {
    const _this = this;
    return (
      <div class="container">
      <div class="counter_bg">
        <div class="row">
          <div class="col-md-4">
            <div class="left_Section">
            <input type="text"  name="item" placeholder  ="Enter Item Name and Click Add" value={this.state.item}   onChange={event => {
                    this.setState({ item:  event.target.value })
                }}
                onKeyDown ={ event => {
                  if (event.key === 'Enter') {
                    this.saveButton();
                  }
              }} 
                
                style={{style }}/>              
              <button type="submit" className="btn btn-primary" onClick={this.saveButton}>Add</button>
              <div class="list_box">
              <ul>
              { ( this.state.list1_items != '')? this.state.list1_items.map(function(items, i){
                 const list = <li id = {items.id} key={i} onClick={_this.leftItemClick.bind(_this, items)} style={{ backgroundColor: ( _this.state.selectedItem != '' && _this.state.selectedItem == items.id) ? _this.state.color :"white" }} 
                  >{items.item_name}</li>
                 return list;
                }):""
              } 
            </ul>
              </div>
            </div>
          </div>
          <div class="col-md-2">
            <div class="button_center">
              <div>
                <button type="button" class="btn btn-primary" onClick={this.lefttoRightButtonClick} disabled= {this.state.leftToRightbtnDisabled}>
                  <i class="fa fa-fw" aria-hidden="true" title="Copy to use arrow-left"></i>
                </button>
              </div>
             <div>
             <button type="button" class="btn btn-primary" onClick={this.rightToLeftButtonClick} disabled=    {this.state.rightToLeftbtnDisabled} >
                <i class="fa fa-fw" aria-hidden="true" title="Copy to use arrow-right"></i>
              </button>
             </div>
              
            </div>
          </div>
          <div class="col-md-4">
            <div class="left_Section">
              <h2>Selected Items</h2>
              <div class="list_box">
              <ul>
               { ( this.state.list2_items != '')? this.state.list2_items.map(function(items, i){
                 const list = <li id = {items.id} key={i} onClick={_this.rightItemClick.bind(_this, items)} style={{ backgroundColor: ( _this.state.selectedItemLeft != '' && _this.state.selectedItemLeft == items.id) ? _this.state.color1 :"white" }} 
                  >{items.item_name}</li>
                 return list;
                }):""
              } 
            </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
        );
  }
}

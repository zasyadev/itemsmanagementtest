<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Item;
use App\Models\List1;
use App\Models\List2;

class ItemController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function getItems()
    {
        $data_list1 = List1::with('items')->get();
        $data_list2 = List2::with('items')->get();
        $left_items = $data_list1->map(function($q) use(&$count){
            $count++;
            return [
                'id' => $q->item_id,
                'item_name' =>  $q->items[0]->items,
                'position' => $q->items[0]->position,

            ];
        });

        $right_items = $data_list2->map(function($q) use(&$count){
            $count++;
            return [
                'id' => $q->item_id,
                'item_name' =>  $q->items[0]->items,
                'position' => $q->items[0]->position,

            ];
        });
        return response()->json([
            'status' => 200,
            'right_items' => $right_items,
            'left_items' => $left_items,

        ]);
    }

     /**
     * add new item
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function addItems( Request $request )
    {      
        $res = false;
        if (  $request->item ) {
            try {
                $record = new Item;
                $record->items = $request->item;
                
                if ( $record->save() ) {
                   
                    $list1_items = List1::orderBy('position', 'desc')->first();
                    $list1 = new List1;
                    $list1->item_id = $record->id;
                    $list1->position = (is_null($list1_items))?'1': ($list1_items->position+1);
                    if($list1->save()){
                        $msg = "Item Added";
                    }
                   

                }
            } catch ( Exception $e ) {
                $msg =  false;
            }
        }
        return response()->json([
            'status' => 200,
            'message' => $msg
        ]);
    }

    /**
     * shift item to right 
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function shiftRight( Request $request )
    {      
        $res = false;
        $msg =  false;
        if (  $request->item_id ) {
            try {
                if(List2::where('item_id',  $request->item_id)->exists()){

                }else{
                    $list1 = List1::where('item_id', $request->item_id )->first();
                    if( $list1){
                    $list1->delete();
                }
                $list2_items = List2::orderBy('position', 'desc')->first();
                $list2 = new List2;
                    $list2->item_id =  $request->item_id;
                    $list2->position = (is_null($list2_items))?'1': ($list2_items->position+1);

                    if($list2->save()){
                        $msg = "Item Added";
                    }
                }
                
                
            } catch ( Exception $e ) {
                $msg =  false;
            }
        }
        return response()->json([
            'status' => 200,
            'message' => $msg
        ]);
    }

     /**
     * shift item to right 
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function shiftleft( Request $request )
    {     
        $res = false;
        if (  $request->item_id ) {
            try {
                $list2 = List2::where('item_id', $request->item_id )->first();
               
                if( $list2){
                    $list2->delete();
                }
                $list1_items = List1::orderBy('position', 'desc')->first();
                $list1 = new List1;
                    $list1->item_id =  $request->item_id;
                    $list1->position = (is_null($list1_items))?'1': ($list1_items->position+1);

                    if($list1->save()){
                        $msg = "Item Added";
                    }
                   

                
            } catch ( Exception $e ) {
                $msg =  false;
            }
        }
        return response()->json([
            'status' => 200,
            'message' => $msg
        ]);
    }

    
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Item;

class List1 extends Model
{
    use SoftDeletes;

   protected $table = 'list1';

   public function items()
    {
        return $this->hasMany(Item::class,'id', 'item_id');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class List2 extends Model
{
    use SoftDeletes;

    protected $table = 'list2';

    public function items()
    {
        return $this->hasMany(Item::class,'id', 'item_id');
    }
    
}
